#ifndef SYNHOW_TARGET_H
#define SYNHOW_TARGET_H
#include "iSynHow.h"

namespace SynHow {
    class Target {
    public:
        std::string name, description;

        static std::unordered_map<std::string, Target*> ByName;
    protected:

    };
};

#endif
