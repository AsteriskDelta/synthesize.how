#ifndef SYNHOW_DOSAGE_H
#define SYNHOW_DOSAGE_H
#include "iSynHow.h"

namespace SynHow {
    class Dosage {
    public:
        resi_t mass;
        std::vector<Symptom*> effects;
    protected:

    };
};

#endif
