#ifndef SYNHOW_SYMPTOM_H
#define SYNHOW_SYMPTOM_H
#include "iSynHow.h"
#include <unordered_map>

namespace SynHow {
    class Symptom {
    public:
        std::string name, commonName, description;

        resi_t danger;
    protected:

    };
};

#endif
