#include "iSynHow.h"
#include <ARKE/Console.h>
#include <ARKE/Application.h>
#include "Server.h"
#include <ARKE/Time.h>
#include "Bindings.h"

#include "User.h"

using namespace SynHow;
int background(int status);

int main(int argc, char** argv) {
    _unused(argc, argv);

    Application::BeginInitialization();
    Application::SetName("Synthesize.How");
    Application::SetCodename("com.d3.synthesizeHow");
    Application::SetCompanyName("Delta III Technologies");
    Application::SetVersion("0.0r1-X");
    Application::SetDataDir("../../data");
    Application::EndInitialization();

    const unsigned short lclPort = 2943;
    Spin::MemberInfo myInfo;
    myInfo.type = Application::GetCodename();
    Spin::MemberID myID(5, lclPort);

    //unsigned short prt = 1384;//1001+rand()%400;
    //Console::Out("Starting on port ", prt);

    HttpServer *server = HttpServer::Active;
    //server->initialize(prt, 8);
    server->endpoint("/test", [](HttpRequest *req) -> HttpReply* {
        delete req;
        HttpReply *ret = new HttpReply();
        ret->set(std::string("it works"), "text");
        return ret;
    });
    BindServerPaths(server);

    Console::Out("Starting Spinster...");
    Spin::Initialize(myID, myInfo);

    Spin::Local->schedule(Spin::Priority::Realtime, background, 0);

    Console::Out("Preparing Server...");
    server->prepare();

    return 0;
}

int background(int status) {
    Console::Out("Paralell BG task created");

    UserInfo rootInfo;
    rootInfo.name.nick = "root";
    User *rootUser = User::Create("root", rootInfo);
    rootUser->permissions.set(Permissions::Root);

    Console::Out("Init completed");

    while(true) {
        Time::Current = time(nullptr);
        usleep(1000);
    }
    return status;
}
