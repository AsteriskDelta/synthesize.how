#include "SynHow.h"
#include "process/MDBImporter.h"
#include "reagent/Reagent.h"
#include <ARKE/Application.h>
#include <cctype>

using namespace arke;
using namespace nvx;
using namespace SynHow;

int main(int argc, char** argv) {
    _unused(argc, argv);

    Application::BeginInitialization();
    Application::SetName("Synthesize.How");
    Application::SetCodename("com.d3.synhow");
    Application::SetCompanyName("Delta III Technologies");
    Application::SetVersion("0.0r1-X");
    Application::SetDataDir("../../data");
    Application::EndInitialization();

    const unsigned short lclPort = 2939;
    Spin::MemberInfo myInfo;
    myInfo.type = Application::GetCodename();
    Spin::MemberID myID(5, lclPort);

    Spin::Initialize(myID, myInfo);

    MDBImporter *importer = new MDBImporter("chem_molecularDatabase.xml");

    importer->import();

    for(auto pair : Index::Reagents) {
        Reagent* reagent = pair.second;
        std::string sub;
        sub += (char(tolower(reagent->id()[0])));
        std::string path = std::string("data/reagents/")+sub + "/" + reagent->id() + ".json";
        std::cout << "saving reagent " << reagent->id() << " to " << path << "\n";
        reagent->save(path);
    }

    return 0;
}
