#ifndef SYNHOW_I_INC_H
#define SYNHOW_I_INC_H
#include <NVX/NVX.h>
#include <Spinster/Spinster.h>
#include <ARKE/ARKE.h>

namespace SynHow {
    using namespace nvx;
    using namespace arke;

    typedef N3T<nvxi_t> vec3_t;
    typedef N2T<nvxi_t> vec2_t;
    typedef NI2<32,32> nvxi_t;

    //Reagent
    class Interaction;
    class Reagent;
    class ReagentClass;
    class Reference;
    class Statemap;
    class Tag;

    //Renderer

    //Route
    class Route;
    class Step;

    //Process
    class MDBImporter;

    //Planner
    class Plan;
    class Planner;

    //Pharmo
    class Association;
    class Dosage;
    class Symptom;
    class Target;

    //Inventory
    class Certification;
    class Equipment;
    class Inventory;
    class Location;
    class Supplier;
}

#endif
