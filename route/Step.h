#ifndef SYNHOW_STEP_H
#define SYNHOW_STEP_H
#include "iSynHow.h"
#include <unordered_map>
//#include "ReagentData.h"
//#include "Equipment.h"
#include "ItemSet.h"
#include <ARKE/Serializable.h>

namespace SynHow {
    struct StepData {
        nvxi_t duration, danger;
        //std::vector<ReagentData> inputs;
        //std::vector<ReagentData> outputs;
        ItemSet inputs, outputs;
        std::vector<Equipment*> equipment;
    };
    class Step : public StepData, public Serializable {
    public:
        std::string text;
        std::string imagePath;
        std::vector<Step*> previous, next;

        virtual void serializeTo(JSONNode *node, const std::string& pth) const override;
        virtual void deserializeFrom(JSONNode *node, const std::string& pth) override;
    protected:

    };
};

#endif
