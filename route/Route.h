#ifndef SYNHOW_ROUTE_H
#define SYNHOW_ROUTE_H
#include "iSynHow.h"
#include "Reagent.h"
#include "Equipment.h"
#include "Step.h"

namespace SynHow {
    class RouteIndex {
    public:
        RouteIndex(RouteIndex *par = nullptr);
        ~RouteIndex();

        void addParent(RouteIndex *parent);
        void addChild(RouteIndex *child);

        void add(Route *route);

        std::vector<Route*> byOutput(Reagent *target);
        std::vector<Route*> byInput(Reagent *target);
    protected:
        std::vector<RouteIndex*> parents, children;

        std::unordered_map<Reagent*, std::vector<Route*>> outputIndex, inputIndex;
    };

    class Route : public StepData {
    public:
        Route();
        virtual ~Route();

        std::string id, name;
        std::string description;

        std::vector<Step> steps;
    protected:
        std::vector<RouteIndex*> indices;
    };
};

#endif
