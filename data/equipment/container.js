{
    "id": "container",
    "name": "Container",
    "type": "",
    "meta": true,
    "attributes": ["volume", "material", "temperature.max", "temperature.min"],
    "cost": {
        "min": 0,
        "max": 0
    },
    "description": ""
}
