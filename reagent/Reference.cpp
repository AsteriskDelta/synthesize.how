#include "Reference.h"

namespace SynHow {
    unsigned long long Reference::ReferenceIDCounter = 0;

    Reference::Reference() : id(std::to_string(ReferenceIDCounter++)) {

    }

    /*void Reference::serializeTo(JSONNode *json, const std::string& path) const {

    }
    void Reference::deserializeFrom(JSONNode *json, const std::string& path) {

    }*/
    std::string Reference::serialize() const {
        char str[512];
        unsigned int typeID = static_cast<unsigned int>(type);
        sprintf(str, "~%d~~%s~~%s~~%s;", typeID, data.c_str(), id.c_str(), url.c_str());
        return std::string(str);
    }
    void Reference::deserialize(const std::string& str) {
        unsigned int typeID;
        char d[512], i[64], u[256];
        sscanf(str.c_str(), "~%d~~%s~~%s~~%s;", &typeID, d, i, u);
        this->type = static_cast<Type>(typeID);
        this->data = std::string(d);
        this->id = std::string(i);
        this->url = std::string(u);
    }

    std::string Reference::TypeName(Type t) {
        if(t == Type::Invalid) return "Invalid";
        else if(t == Type::Article) return "Article";
        else if(t == Type::Textbook) return "Textbook";
        else if(t == Type::Link) return "Link";
        else if(t == Type::Patent) return "Patent";
        else if(t == Type::Anecdote) return "Anecdote";
        else if(t == Type::Synthesis) return "Synthesis";

        return "Invalid";
    }
}
