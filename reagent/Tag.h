#ifndef SYNHOW_TAG_H
#define SYNHOW_TAG_H
#include "iSynHow.h"
#include <unordered_map>

namespace SynHow {
    class Tag {
    public:
        std::string id, name, description;

        static std::unordered_map<std::string, Tag*> ByName;
    protected:

    };
};

#endif
