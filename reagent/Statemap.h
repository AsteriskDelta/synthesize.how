#ifndef SYNHOW_STATEMAP_H
#define SYNHOW_STATEMAP_H
#include "iSynHow.h"

namespace SynHow {
    class StateMap {
    public:
        class Point : public vec3_t {
            inline auto pressure() const {
                return (*this)[0];
            }
            inline auto temperature() const {
                return (*this)[0];
            }
            inline auto density() const {
                return (*this)[0];
            }
        };

        std::vector<Point> points;

        void add(const vec3_t& v);
        Point sample(const vec3_t& pt);
    protected:

    };
};

#endif
