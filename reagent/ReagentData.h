#ifndef SYNHOW_REAGENT_DATA_H
#define SYNHOW_REAGENT_DATA_H
#include "iSynHow.h"

namespace SynHow {
    class Reagent;
    class ReagentData {
    public:

        Reagent *reagent;
        nvxi_t mass;
        nvxi_t rate;
        nvxi_t efficiency;
        nvxi_t expiration;
    protected:

    };
}

#endif
