#ifndef SYNHOW_INTERACTION_H
#define SYNHOW_INTERACTION_H
#include "iSynHow.h"

namespace SynHow {
    class Interaction {
    public:
        Interaction();
        virtual ~Interaction();

        std::vector<Reagent*> reagents;
        std::string description;
    protected:

    };
};

#endif
