#ifndef SYNHOW_REFERENCE_H
#define SYNHOW_REFERENCE_H
#include "iSynHow.h"
#include <ARKE/Serializable.h>

namespace SynHow {
    class Reference : public Serializable {
    public:
        enum Type {
            Invalid,
            Article,
            Textbook,
            Link,
            Patent,
            Anecdote,
            Synthesis
        };

        Reference();

        Type type;
        std::string data;
        std::string id, url;

        //virtual void serializeTo(JSONNode *json, const std::string& path) const override;
        //virtual void deserializeFrom(JSONNode *json, const std::string& path) override;

        virtual std::string serialize() const override;
        virtual void deserialize(const std::string& str) override;

        static unsigned long long ReferenceIDCounter;
        static std::string TypeName(Type t);
    protected:

    };
};

#endif
