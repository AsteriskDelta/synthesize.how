#ifndef SYNHOW_INTERACTION_H
#define SYNHOW_INTERACTION_H
#include "iSynHow.h"
#include <unordered_map>

namespace SynHow {
    class Interaction {
    public:
        std::string id;
        std::vector<Reagent*> participants;

        std::string effect;
    protected:

    };
};

#endif
