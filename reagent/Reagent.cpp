#include "Reagent.h"
#include <ARKE/JSON.h>
#include "ReagentClass.h"
#include "Reference.h"
#include "Statemap.h"
#include "Tag.h"
#include "Route.h"
#include "Certification.h"

namespace SynHow {
    static std::vector<std::string> VectorizeSet(std::unordered_set<std::string> set) {
        std::vector<std::string> ret;
        for(const auto& it : set) ret.push_back(it);
        return ret;
    }

    static std::unordered_set<std::string> DevectorizeSet(const std::vector<std::string> &vec) {
        std::unordered_set<std::string> ret;
        for(const auto& str : vec) ret.insert(str);
        return ret;
    }

    namespace Index {
        AutoIndexer<Reagent> Reagents;
    };

    Reagent::Reagent() {

    }
    Reagent::~Reagent() {

    }

    bool Reagent::save(const std::string& pth) {
        JSON json;// = new JSON();
        json.blank();
        json.set(name, "name");
        json.set(shortName, "short_name");
        json.set(description, "description");
        json.set(formula, "formula");

        json.array<std::string>(VectorizeSet(drugbankIDs), "drugbank_id");
        json.array<std::string>(VectorizeSet(brandNames), "brand_names");
        json.array<std::string>(VectorizeSet(altNames), "alt_names");

        json.set(casNumber, "cas_number");
        json.set(unii, "unii");
        json.set(type.id, "type");
        json.set(sequence, "sequence");

        {
            std::vector<std::string> prodVector;
            for(Route *const& route : producers) prodVector.push_back(route->id);
            json.array<std::string>(prodVector, "producers");
        }
        {
            std::vector<std::string> conVector;
            for(Route *const& route : consumers) conVector.push_back(route->id);
            json.array<std::string>(conVector, "consumers");
        }

        {
            std::vector<std::string> prodVector;
            for(auto *const& route : requirements) prodVector.push_back(route->id);
            json.array<std::string>(prodVector, "requirements");
        }
        {
            std::vector<std::string> prodVector;
            for(auto *const& route : references) prodVector.push_back(route->serialize());
            json.array<std::string>(prodVector, "references");
        }
        {
            std::vector<std::string> prodVector;
            for(auto *const& route : tags) prodVector.push_back(route->id);
            json.array<std::string>(prodVector, "tags");
        }

        {
            std::vector<std::string> prodVector;
            for(auto *const& route : interactions) prodVector.push_back(route->id);
            json.array<std::string>(prodVector, "interactions");
        }

        json.set(fdaLabel, "fda_label");
        json.set(msds, "msds");

        json.create("experimental");
        json.create("pharmo");

        json.set(double(experimental.meltingPoint), "experimental.melting_point");
        json.set(double(experimental.hydrophobicity), "experimental.hydrophobicity");
        json.set(double(experimental.isoelectricPoint), "experimental.isoelectric_point");
        json.set(double(experimental.molecularWeight), "experimental.molecular_weight");
        json.set(double(experimental.ph), "experimental.ph");

        json.set(double(experimental.ld50), "ld50");
        json.set(double(experimental.ed50), "ed50");
        json.set(double(experimental.ec50), "ed50");

        json.set(pharmo.indiction, "pharmo.indiction");
        json.set(pharmo.dynamics, "pharmo.dynamics");
        json.set(pharmo.action, "pharmo.action");
        json.set(pharmo.toxicity, "pharmo.toxicity");
        json.set(pharmo.metabolism, "pharmo.metabolism");

        json.set(double(pharmo.halfLife), "pharmo.half_life");
        json.set(double(pharmo.pricePerGram), "pharmo.price_per_gram");

        json.set(pharmo.routeOfElimination, "pharmo.route_of_elimination");
        json.set(pharmo.volumeOfDistributionRaw, "pharmo.vod_raw");
        json.set(pharmo.clearanceRaw, "pharmo.clearance_raw");
        json.set(pharmo.halfLifeRaw, "pharmo.half_life_raw");

        std::cout << "saved " << pth << "\n";
        json.save(pth);
    }
    bool Reagent::load(const std::string& pth) {
        JSON json;// = new JSON();
        json.load(pth);

        name = json.get<std::string>("name");
        shortName = json.get<std::string>("short_name");
        description = json.get<std::string>("description");
        formula = json.get<std::string>("formula");

        casNumber = json.get<std::string>("cas_number");
        unii = json.get<std::string>("unii");

        experimental.meltingPoint = nvxi_t(json.get<double>("experimental.melting_point"));
        experimental.hydrophobicity = nvxi_t(json.get<double>("experimental.hydrophobicity"));
        experimental.isoelectricPoint = nvxi_t(json.get<double>("experimental.isoelectric_point"));
        experimental.molecularWeight = nvxi_t(json.get<double>("experimental.molecular_weight"));
        experimental.ph = nvxi_t(json.get<double>("experimental.ph"));
        experimental.ld50 = nvxi_t(json.get<double>("experimental.ld50"));
        experimental.ed50 = nvxi_t(json.get<double>("experimental.ed50"));
        experimental.ec50 = nvxi_t(json.get<double>("experimental.ec50"));

        pharmo.indiction = json.get<std::string>("pharmo.indiction");
        pharmo.dynamics = json.get<std::string>("pharmo.dynamics");
        pharmo.action = json.get<std::string>("pharmo.action");
        pharmo.toxicity = json.get<std::string>("pharmo.toxicity");
        pharmo.metabolism = json.get<std::string>("pharmo.metabolism");
        pharmo.routeOfElimination = json.get<std::string>("pharmo.route_of_elimination");
        pharmo.volumeOfDistributionRaw = json.get<std::string>("pharmo.vod_raw");
        pharmo.clearanceRaw = json.get<std::string>("pharmo.clearance_raw");
        pharmo.halfLifeRaw = json.get<std::string>("pharmo.half_life_raw");

        fdaLabel = json.get<std::string>("fda_label");
        msds = json.get<std::string>("msds");

        drugbankIDs = DevectorizeSet(json.array<std::string>("drugbank_id"));
        brandNames = DevectorizeSet(json.array<std::string>("brand_names"));
        altNames = DevectorizeSet(json.array<std::string>("alt_names"));

        linkData = new LinkData();

        static const std::string rtags[] = {"producers", "consumers", "references", "requirements", "tags"};
        unsigned int i = 0;
        for(std::list<std::string> *ptr = &linkData->producers; i < sizeof(rtags)/sizeof(std::string); ptr++, i++) {
            for(auto it = json.getNode(rtags[i]).begin(); it != json.getNode(rtags[i]).end(); ++it) {
                if(i == 2) {
                    Reference *ref = new Reference();
                    ref->deserialize(it->get<std::string>());
                    references.push_back(ref);
                } else {
                    ptr->push_back(it->get<std::string>());
                }
            }
        }

        return true;
    }

    bool Reagent::link() {

        delete linkData;
        return true;
    }

    void Reagent::AddIndexFor(Reagent *reagent) {
        Index::Reagents.add(reagent);
    }
}
