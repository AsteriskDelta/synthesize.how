#ifndef SYNHOW_REAGENT_H
#define SYNHOW_REAGENT_H
#include "iSynHow.h"
#include <unordered_set>
#include "Reference.h"
#include "ReagentClass.h"
#include "Interaction.h"
#include "AutoIndexer.h"

namespace SynHow {
    class Reagent : public IndexedObject {
    public:
        Reagent();
        virtual ~Reagent();

        std::string name, shortName;
        std::string description;

        std::string formula;
        std::unordered_set<std::string> drugbankIDs;
        std::unordered_set<std::string> brandNames;
        std::unordered_set<std::string> altNames;
        std::string casNumber;
        std::string unii;

        ReagentClass type;
        std::string sequence;

        std::vector<Route*> producers;
        std::vector<Route*> consumers;

        std::vector<Certification*> requirements;
        std::vector<Reference*> references;
        std::unordered_set<Tag*> tags;

        struct {
            nvxi_t meltingPoint;//kelvin
            nvxi_t hydrophobicity;
            nvxi_t isoelectricPoint;
            nvxi_t molecularWeight;
            nvxi_t ph;
            nvxi_t ld50, ed50, ec50;
        } experimental;

        struct {
            std::string indiction, dynamics, action;
            std::string toxicity;
            std::string metabolism;
            nvxi_t halfLife;

            nvxi_t pricePerGram;

            std::string routeOfElimination;

            std::string volumeOfDistributionRaw;
            std::string clearanceRaw;
            std::string halfLifeRaw;
        } pharmo;

        std::vector<Interaction*> interactions;

        std::string fdaLabel;
        std::string msds;

        struct LinkData {
            std::list<std::string> producers, consumers, references, requirements, tags;
        };
        LinkData *linkData;

        inline virtual std::string id() const override {
            return this->name;
        }

        virtual bool save(const std::string& pth);
        virtual bool load(const std::string& pth);
        virtual bool link();

        static void AddIndexFor(Reagent *reagent);
    protected:

    };

    namespace Index {
        extern AutoIndexer<Reagent> Reagents;
    }
    /*
    struct ReagentData {
        Reagent *reagent;
        double mass;
        nvxi_t efficiency;
        nvxi_t expiration;
    };*/
};

#endif
