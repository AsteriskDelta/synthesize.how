#ifndef SYNHOW_MDBI_INC_H
#define SYNHOW_MDBI_INC_H
#include "iSynHow.h"
#include <ARKE/XML.h>

namespace SynHow {
    class MDBImporter {
    public:
        MDBImporter(const std::string& path);
        virtual ~MDBImporter();

        void import();
    protected:
        XMLDoc *doc;
    };
}

#endif
