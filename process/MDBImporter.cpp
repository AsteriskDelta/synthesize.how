#include "MDBImporter.h"
#include <ARKE/Console.h>
#include "Reagent.h"
#include "Reference.h"
#include "MiscStr.h"

namespace SynHow {
    MDBImporter::MDBImporter(const std::string& path) {
        Console::Out("MDBImporter: importing ",path,"...");
        doc = new XMLDoc(path);
        Console::Out("MDBImporter: imported ", path);
    }
    MDBImporter::~MDBImporter() {

    }

    void MDBImporter::import() {
        //Console::Out("Importing, first tag = ",doc->child().name(),", second = ", doc->child().sibling().name(), ", third=",doc->child().sibling().sibling().name());
        //Console::Out("Drug tag name: ",doc->child("drug").name());
        for(XMLNode drug = doc->child("drugbank").child("drug"); bool(drug); ++drug) {
            const std::string name = drug.child("name").get<std::string>();

            Console::Out(name);
            Reagent *reagent = Index::Reagents[name];//new Reagent();
            bool exists = reagent != nullptr;
            if(!exists) reagent = new Reagent();

            reagent->name = name;
            //reagent->shortName = ;
            reagent->description = drug.child("description")? drug.child("description").get<std::string>() : "";

            reagent->casNumber = drug.child("cas-number")? drug.child("cas-number").get<std::string>() : "";
            reagent->unii = drug.child("unii")? drug.child("unii").get<std::string>() : "";

            for(XMLNode drugbankID = drug.child("drugbank-id"); bool(drugbankID); ++drugbankID) {
                reagent->drugbankIDs.insert(drugbankID.get<std::string>());
            }
            for(XMLNode node = drug.child("mixtures").child("mixture"); bool(node); ++node) {
                reagent->brandNames.insert(node.child("name").get<std::string>());
            }
            for(XMLNode node = drug.child("synonyms").child("synonym"); bool(node); ++node) {
                reagent->altNames.insert(node.get<std::string>());
            }

            reagent->sequence = (drug.child("sequences") && drug.child("sequences").child("sequence") )?
            drug.child("sequences").child("sequence").get<std::string>() : "";

            reagent->pharmo.indiction = drug.child("indiction")? drug.child("indiction").get<std::string>() : "";
            reagent->pharmo.dynamics = drug.child("pharmacodynamics")? drug.child("pharmacodynamics").get<std::string>() : "";
            reagent->pharmo.action = drug.child("mechanism-of-action")? drug.child("mechanism-of-action").get<std::string>() : "";
            reagent->pharmo.toxicity = drug.child("toxicity")? drug.child("toxicity").get<std::string>() : "";
            reagent->pharmo.metabolism = drug.child("metabolism")? drug.child("metabolism").get<std::string>() : "";
            reagent->pharmo.routeOfElimination = drug.child("route-of-elimination")? drug.child("route-of-elimination").get<std::string>(): "";

            reagent->pharmo.volumeOfDistributionRaw = drug.child("volume-of-distribution")? drug.child("volume-of-distribution").get<std::string>() : "";
            reagent->pharmo.clearanceRaw = drug.child("clearance")? drug.child("clearance").get<std::string>() : "";

            reagent->pharmo.halfLifeRaw = drug.child("half-life")? drug.child("half-life").get<std::string>() : "";
            reagent->pharmo.halfLife = Misc::FirstNumber(reagent->pharmo.halfLifeRaw);

            for(XMLNode node = drug.child("experimental-properties").child("property"); bool(node); ++node) {
                const std::string kind = node.child("kind").get<std::string>();
                const std::string value = node.child("value").get<std::string>();

                if(kind == "Melting Point") {
                    reagent->experimental.meltingPoint = Misc::FirstNumber(value);
                } else if(kind == "Hydrophobicity") {
                    reagent->experimental.hydrophobicity = Misc::FirstNumber(value);
                } else if(kind == "Isoelectric Point") {
                    reagent->experimental.isoelectricPoint = Misc::FirstNumber(value);
                } else if(kind == "Molecular Weight") {
                    reagent->experimental.molecularWeight = Misc::FirstNumber(value);
                } else if(kind == "Molecular Formula") {
                    reagent->formula = node.child("value");
                }
            }

            reagent->fdaLabel = drug.child("fda-label").get<std::string>();
            reagent->msds = drug.child("msds").get<std::string>();

            for(XMLNode node = drug.child("general-references").child("articles").child("article"); bool(node); ++node) {
                const std::string contents = node.child("citation").get<std::string>();
                if(contents.size() < 3) continue;

                Reference *ref = new Reference();
                ref->type = Reference::Article;
                ref->data = contents;
                ref->id = node.child("pmid").get<std::string>();
                reagent->references.push_back(ref);
            }
            for(XMLNode node = drug.child("general-references").child("textbooks").child("textbook"); bool(node); ++node) {
                const std::string contents = node.child("citation").get<std::string>();
                if(contents.size() < 3) continue;

                Reference *ref = new Reference();
                ref->type = Reference::Textbook;
                ref->data = contents;
                ref->id = node.child("isbn").get<std::string>();
                reagent->references.push_back(ref);
            }
            for(XMLNode node = drug.child("general-references").child("links").child("link"); bool(node); ++node) {
                const std::string contents = node.child("title").get<std::string>();
                if(contents.size() < 3) continue;

                Reference *ref = new Reference();
                ref->type = Reference::Link;
                ref->data = contents;
                ref->id = ref->url = node.child("url").get<std::string>();
                reagent->references.push_back(ref);
            }
            for(XMLNode node = drug.child("synthesis-reference"); bool(node); ++node) {
                const std::string contents = node.get<std::string>();
                if(contents.size() < 3) continue;

                Reference *ref = new Reference();
                ref->type = Reference::Synthesis;
                ref->data = contents;
                reagent->references.push_back(ref);
            }
            for(XMLNode node = drug.child("patents").child("patent"); bool(node); ++node) {
                const std::string contents = node.child("country").get<std::string>();
                if(contents.size() < 3) continue;

                Reference *ref = new Reference();
                ref->type = Reference::Patent;
                ref->data = contents;
                ref->id = ref->url = node.child("number").get<std::string>();
                reagent->references.push_back(ref);
            }

            Reagent::AddIndexFor(reagent);
        }
    }

};
