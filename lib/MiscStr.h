#ifndef ARKE_MISC_STR_H
#define ARKE_MISC_STR_H
#include "iSynHow.h"

namespace arke {
    namespace Misc {
        std::string FirstNumberString(const std::string &str);
        double FirstNumber(const std::string &str);
    }
}

#endif
