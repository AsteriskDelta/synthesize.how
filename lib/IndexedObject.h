#ifndef ARKE_INDEXED_OBJECT_H
#define ARKE_INDEXED_OBJECT_H
#include "iSynHow.h"

namespace arke {
    class IndexedObject {
    public:
        IndexedObject();
        virtual ~IndexedObject();

        virtual std::string id() const;

        //semistatic:
        virtual unsigned int categoryCount() const;
        virtual std::string category(unsigned int off) const;
        virtual std::string categoryID(unsigned int off) const;

        virtual unsigned int fieldCount(unsigned int cat) const;
        virtual std::string field(unsigned int cat, unsigned int off) const;
        virtual std::string fieldName(unsigned int off) const;
    protected:
        virtual void onRegister();
        virtual void onUnregister();
    };
};

#endif
