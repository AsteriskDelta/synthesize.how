#include "IndexedObject.h"

namespace arke {
    IndexedObject::IndexedObject() {

    }
    IndexedObject::~IndexedObject() {

    }

    std::string IndexedObject::id() const {

    }

    //semistatic:
    unsigned int IndexedObject::categoryCount() const {

    }
    std::string IndexedObject::category(unsigned int off) const {

    }
    std::string IndexedObject::categoryID(unsigned int off) const {

    }

    unsigned int IndexedObject::fieldCount(unsigned int cat) const {

    }
    std::string IndexedObject::field(unsigned int cat, unsigned int off) const {

    }
    std::string IndexedObject::fieldName(unsigned int off) const {

    }

    void IndexedObject::onRegister() {

    }
    void IndexedObject::onUnregister() {

    }
}
