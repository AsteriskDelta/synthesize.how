#include "MiscStr.h"
#include <algorithm>
#include <stdlib.h>

namespace arke {
    namespace Misc {
        std::string FirstNumberString(const std::string & str) {
          std::size_t const n = str.find_first_of("0123456789");
          if(n != std::string::npos) {
            std::size_t const m = str.find_first_not_of("0123456789.x", n);
            return str.substr(n, m != std::string::npos ? m-n : m);
          }
          return std::string();
        }

        double FirstNumber(const std::string & str) {
            const std::string sub = FirstNumberString(str);
            return strtod(sub.c_str(), nullptr);
        }
    }
}
