#ifndef ARKE_AUTOINDEXER_H
#define ARKE_AUTOINDEXER_H
#include "IndexedObject.h"
#include <unordered_map>
#include <unordered_set>

namespace arke {
    template<typename T>
    class AutoIndexer {
    public:
        std::unordered_map<std::string, T*> byID;
        struct Category {
            std::string id;
            std::vector<std::string> fields;
        };
        std::vector<Category> categories;

        std::string dbPath;

        inline AutoIndexer() {

        }
        inline virtual ~AutoIndexer() {

        }

        inline virtual T* get(const std::string& id) {
            return byID[id];
        }
        inline virtual T* operator[](const std::string& id) {
            return this->get(id);
        }

        inline virtual void add(T* obj) {
            byID[obj->id()] = obj;
            for(auto& category : categories) {
                for(std::string& fieldName : category.fields) {

                }
            }
        }
        inline virtual void remove(T* obj) {
            byID.erase(obj->id());
        }

        inline virtual T* load(const std::string& id) {
            std::string path = this->dbPath + "/" + this->filePath(id);
        }

        inline virtual bool synchronize() {
            for(T* obj : dirty) {
                obj->save(this->filePath(obj));
            }
            dirty.clear();
        }

        inline virtual void setDirty(T* obj) {
            dirty.insert(obj);
        }

        inline auto begin() {
            return byID.begin();
        }
        inline auto end() {
            return byID.end();
        }

    protected:
        std::unordered_set<T*> dirty;

        inline virtual std::string filePath(const std::string& id) {
            return this->filePath(byID[id]);
        }
        inline virtual std::string filePath(T* obj) const {

        }
    };
};

#endif
