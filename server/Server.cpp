#include "Server.h"
#include "Session.h"
#include <ARKE/Server.impl.h>


namespace SynHow {
    HttpServer *mainServer;
    struct MakeServer {
        MakeServer() {
            std::cout << "Making server...\n";
            mainServer = new HttpServer(1384, 8);
            HttpServer::Active = mainServer;
        }
    };
    MakeServer  _dalloc_pre() _used makeServer;
}
