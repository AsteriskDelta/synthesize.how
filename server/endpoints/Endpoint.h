#ifndef ARKE_ENDPOINT_H
#define ARKE_ENDPOINT_H
#include "Server.h"

namespace arke {
    namespace RestServer {
        class Endpoint {
        public:
            Endpoint(const std::string& path, HttpEndpoint fn);
        protected:

        };
    }
};

#endif
