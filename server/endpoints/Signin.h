#ifndef SYNHOW_SIGNIN_H
#define SYNHOW_SIGNIN_H
#include "Endpoint.h"

namespace SynHow {
    HttpReply *Signin(HttpRequest *request);
}

#endif
