#include "Signin.h"
#include "User.h"
#include "Session.h"

namespace SynHow {
    HttpReply *Signin(HttpRequest *request) {
        //std::cout << "Splats : ";
        //for(const auto& splat : request->splats) std::cout << splat << ", ";
        //std::cout << "\n";
        HttpReply *ret = new HttpReply();

        std::string idSplat = "";
        if(request->splats.size() > 0) idSplat = request->splats[0];

        User *user = User::GetByID(idSplat);
        if(user == nullptr) user = User::GetByEmail(idSplat);

        if(user != nullptr) {
            ret->set(int64_t(1), std::string("success"));
            Session::Current->user = user;
        } else {
            ret->set(int64_t(0), std::string("success"));
        }
        return ret;
    }
    Endpoint signin  _used _dalloc(2500) ("/signin/*", Signin);
}
