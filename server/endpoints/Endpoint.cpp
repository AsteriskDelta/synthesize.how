#include "Endpoint.h"

namespace arke {
    namespace RestServer {
        Endpoint::Endpoint(const std::string& path, HttpEndpoint fn) {
            std::cout << "add endpoint for " << path << "\n";
            HttpServer::Active->endpoint(path, fn);
        }
    }
}
