#ifndef SYNHOW_PLAN_H
#define SYNHOW_PLAN_H
#include "iSynHow.h"
#include "Route.h"

namespace SynHow {

    struct PlanScore {
        union {
            struct {
                nvxi_t duration;
                nvxi_t danger;
                nvxi_t complexity;
                nvxi_t price;
                nvxi_t stocked;
            };
            nvxi_t values[5];
        };
        PlanScore() : values() {
        }
        PlanScore(const PlanScore &o) : values {
            for(int i = 0; i < 5; i++) values[i] = o.values[i];
        }

        inline PlanScore operator+(const PlanScore& o) const {
            Planscore ret = *this;
            for(int i = 0; i < 5; i++) ret.values[i] = values[i] + o.values[i];
            return ret;
        }
        inline PlanScore operator*(const PlanScore& o) const {
            Planscore ret = *this;
            for(int i = 0; i < 5; i++) ret.values[i] = values[i] * o.values[i];
            return ret;
        }
        inline nvxi_t sum() const {
            nvxi_t ret = 0;
            for(int i = 0; i < 5; i++) ret += values[i];
        }
        inline nvxi_t evalute(const PlanScore& priors) const {
            return ((*this) * priors).sum();
        }
    };

    class Plan : public PlanScore {
    public:
        Plan();
        virtual ~Plan();

        std::vector<Plan*> requisites;
        Plan *parent;

        ItemSet usage;
        Route *route;

        bool complete;

        inline bool frontier() const {
            return requisites.empty();
        }
        inline bool exhausted() const {
            return this->frontier() && this->complete;
        }
    protected:

    };
};

#endif
