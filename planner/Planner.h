#ifndef SYNHOW_PLANNER_H
#define SYNHOW_PLANNER_H
#include "iSynHow.h"
#include "ItemSet.h"
#include "Plan.h"

namespace SynHow {

    class Planner {
    public:
        User *user;
        Inventory *inventory;

        ItemSet desired;

        PlanScore priorities;

        Plan *goal;
        std::vector<Plan*> frontier;
    protected:

    };
};

#endif
