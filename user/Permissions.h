#ifndef ARKE_PERMISSIONS_H
#define ARKE_PERMISSIONS_H

namespace arke {
    class Permissions {
    public:
        typedef uint32_t underlying_t;
        enum Bits {

            User = 0x0,
            Moderator = 0x0 | User,
            Admin = 0x0 | Moderator,
            Root = 0x0 | Admin
        };

        underlying_t data;

        inline bool has(underlying_t f) const {
            return (data & f) != 0x0;
        }
        inline void set(underlying_t f) {
            data |= f;
        }
        inline void clear(underlying_t f) {
            data &= ~f;
        }
    protected:

    };
}

#endif
