#include "User.h"

namespace arke {
    std::unordered_map<std::string, User*>  User::ByID, User::ByEmail;

    User::User(const std::string& nid, const UserInfo& par) : UserInfo(par), id(nid) {

    }
    User* User::Create(const std::string& id, const UserInfo& info) {
        User *user = new User(id, info);
        ByID[id] = user;
        if(!user->email.empty()) ByEmail[user->email] = user;
        return user;
    }
    User* User::GetByID(const std::string& idStr) {
        auto it = ByID.find(idStr);
        if(it == ByID.end()) return nullptr;
        else return it->second;
    }
    User* User::GetByEmail(const std::string& idStr) {
        auto it = ByEmail.find(idStr);
        if(it == ByEmail.end()) return nullptr;
        else return it->second;
    }
}
