#ifndef SYNHOW_USER_H
#define SYNHOW_USER_H
#include "iSynHow.h"
#include "Permissions.h"

namespace arke {
    using namespace SynHow;

    struct UserInfo {
        struct {
            std::string nick, first, last;
        } name;
        std::string email;
    };

    class User : public UserInfo {
    public:
        std::string id;
        std::vector<Location*> locations;

        std::string password;
        Permissions permissions;

        User(const std::string& id, const UserInfo& par);

        void setPassword(const std::string& plainText);
        bool authenticate(const std::string& pass);

        static User* Create(const std::string& id, const UserInfo& info);
        static User* GetByID(const std::string& idStr);
        static User* GetByEmail(const std::string& emailStr);
        static std::unordered_map<std::string, User*> ByID, ByEmail;
    protected:
        void registerSelf();
    };
}

#endif
