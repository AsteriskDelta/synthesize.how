#ifndef SYNHOW_ITEM_H
#define SYNHOW_ITEM_H
#include "iSynHow.h"

namespace SynHow {
    class Location;
    class Equipment;
    class Reagent;

    class Item {
    public:
        Item();
        virtual ~Item();

        enum Type {
            t_Invalid,
            t_Reagent,
            t_Equipment
        };
        Type type;

        Equipment *equipment;
        Reagent *reagent;

        Location *location;

        double mass, volume;
    protected:

    };
};

#endif
