#ifndef SYNHOW_SUPPLIER_H
#define SYNHOW_SUPPLIER_H
#include "iSynHow.h"

namespace SynHow {
    class Supplier {
    public:
        std::string name, url;
        std::vector<Certification*> requirements;
    protected:

    };
};

#endif
