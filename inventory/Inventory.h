#ifndef SYNHOW_INVENTORY_H
#define SYNHOW_INVENTORY_H
#include "iSynHow.h"
#include "Reagent.h"
#include "Equipment.h"
#include "Location.h"
#include "ItemSet.h"

namespace SynHow {
    class Inventory : public Location, public ItemSet {
    public:
        Inventory();
        virtual ~Inventory();

        //std::vector<Location*> locations;
    protected:

    };
};

#endif
