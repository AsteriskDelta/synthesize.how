#ifndef SYNHOW_ITEM_SET_H
#define SYNHOW_ITEM_SET_H
#include "iSynHow.h"

namespace SynHow {
    class Item;

    class ItemSet : public std::vector<Item*> {
    public:
        bool has(const ItemSet &o);

        nvxi_t consumableValue() const;
        nvxi_t equipmentValue() const;

        std::vector<ItemSet*> children;
    protected:

    };
}

#endif
