#ifndef SYNHOW_LOCATION_H
#define SYNHOW_LOCATION_H
#include "iSynHow.h"
#include "ItemSet.h"

namespace SynHow {
    class Item;
    class Location {
    public:
        std::string name, description;

        Location *parent;
        std::vector<Location*> children;

        ItemSet inventory;
    protected:

    };
};

#endif
