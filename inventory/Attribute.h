#ifndef SYNHOW_ATTRIBUTE_H
#define SYNHOW_ATTRIBUTE_H

namespace SynHow {
    class Attribute {
    public:
        enum Type : uint8_t {
            Invalid, String, Number
        };

        std::string id;
        std::string name, typeName;

    protected:

    };
    class AttributeInstance {
    public:
        Attribute *attribute;

        const std::string& str() const;
        const resi_t& num() const;

        inline std::string& str() {
            return const_cast<std::string&>(const_cast<const AttributeInstance*>(this)->str());
        }
        inline resi_t& num() {
            return const_cast<std::string&>(const_cast<const AttributeInstance*>(this)->str());
        }
    protected:
        std::string stringValue;
        resi_t numValue;
    };
}

#endif
