#ifndef SYNHOW_EQUIPMENT_H
#define SYNHOW_EQUIPMENT_H
#include "iSynHow.h"

namespace SynHow {
    class Equipment {
    public:
        Equipment();
        virtual ~Equipment();

        std::string id, name, type;
        std::string description;
        bool meta;
        struct {
            nvxi_t min, max;
        } cost;

        virtual bool save(const std::string& pth);
        virtual bool load(const std::string& pth);
        virtual bool link();
    protected:

    };

    namespace Index {
        extern AutoIndexer<::SynHow::Equipment> Equpment;
    }
};

#endif
