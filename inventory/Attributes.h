#ifndef SYNHOW_ATTRIBUTES_H
#define SYNHOW_ATTRIBUTES_H
#include <ARKE/SmallMap.h>
#include "Attribute.h"

namespace SynHow {
    class Attributes : public SmallMap<Attribute*, AttributeValue> {
    public:

    protected:

    };
}

#endif
