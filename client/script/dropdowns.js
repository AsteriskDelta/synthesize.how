Dropdowns = function() {

};
Dropdowns.Plan = null;

$(function() {
    $("body").append("<div id='authDropdown' class=\"dropDown topButton hidden\"></div>");

    Auth.Dropdown = WMKit.Register("authDropdown");

    Auth.Dropdown.exclusive = true;
    Auth.Dropdown.forceRegen = true;

    Auth.Dropdown.generator = function() {
    var ret = "<div onclick=\"WMKit.Hide('authDropdown')\">Account</div><ul>";

        if(Auth.LoggedIn) {
            ret += `<li onclick="">My Account</li>`;
            ret += `<li class="empty"></li><li onclick="Auth.Logout();">Logout</li>`
        } else {
            ret += `<li onclick="WMKit.Toggle('authLogin');">Login</li>`;
            ret += `<li onclick="WMKit.Toggle('authRegister');">Register</li>`;
        }

        ret += "</ul>"

        return ret;
    };

    Dropdowns.Plan = WMKit.Register("planDropdown");

    Dropdowns.Plan.exclusive = true;
    Dropdowns.Plan.forceRegen = false;

});
